#ifndef _CWG_LIBS_H_
#define _CWG_LIBS_H_

/*================================================================================================
This file contains all the #includes from depentent libraries and some #define for further use
-Chuan Wang- 2013/6/23
================================================================================================*/

//System + STL:
#include <vector>
#include <windows.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <limits>  
#include <direct.h>
#include <iostream>
#include <sstream>
#include <time.h>
#include <algorithm>
#include <map>
#include <cassert>
#include <iomanip>
#include <typeinfo>

//OpenCV: OpenCV library must be compiled in unmanaged manner.
#include <opencv/cv.h> 
#include <opencv2/opencv.hpp>

#include <sparse_matrix.h>
#include <SparseSolver.h>

//Others:
using namespace std;
using namespace cv;

//#define:
//#define Function Type:
#define API_FUNC //for get/set properties
#define TST_FUNC //for test a function
#define VIS_FUNC //for visualize results

//#define Common Numbers:
#define CV_EPSILON 2.2204e-16
#define CV_PRE_ASSIGN 10000000
#define NUMBER_PI 3.1415926535897932384626433832795

#define SAFE_DELETE(ptr) if(ptr){delete ptr; ptr=NULL;}
#define SAFE_DELETE_ARRAY if(ptr){delete [] ptr; ptr=NULL;}

#endif