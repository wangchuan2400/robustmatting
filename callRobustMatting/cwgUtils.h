#ifndef _CWG_UTILS_H_
#define _CWG_UTILS_H_

#include "cwgLibs.h"
#include "dirent.h"
/************************************************************************/
/* This file contains all the APIs of common tools
*/
/************************************************************************/
#pragma region utility_data

extern Mat glbColorMapJet;

#pragma endregion utility_data

#pragma region utility_functions
/************************************************************************/ 
/* PART I - ALL UTILS FUNCS.
*/
/************************************************************************/
// System IO related
// show all filenames with specified suffix contained by folder whose name is folder_name.
vector<string>			show_file_list(const string &folder_name, const string &suffix);
// show all dirnames with specified prefix contained by folder whose name is folder_name.
vector<string>			show_subdir_list(const string& folder_name, const string& subfolder_prefix);
// return whether a file exists.
bool					is_file_exist(const string& filename);
// return the number of bytes of a file. if the file cannot be opened, return -1;
int						file_length(const string& filename);
// replace the suffix of filename. return the output filename. The original file will not be changed.
string					replace_suffix_name(string inputfilename, string suffix);

// Math
double					dround(double r);
Mat						closest_rank1(const Mat& H, const Mat& origH);

// Other
void					pexit(string errorinfo);

template<class T>
string					num2str(T num, int precision=-1)
{
	stringstream ss; 
	if (precision != -1)
		ss << std::setprecision(precision);
	ss << num;
	return ss.str(); 
}


// OpenCV
Mat						imchessboard(int height, int width);
Mat						cvt_to_rgba(Mat img, bool is_transparent);
Mat						to_fixed_color(Mat img, Scalar color);

Mat						loadmat(const string& filename);
Mat						loadmat(ifstream& ifile);
bool					writemat(const string& filename, Mat A);
bool					writemat(ofstream& ofile, Mat A);
bool					coutmat(const string& filename, Mat A);

// Visualize:
Vec3b					color_map( int index, int nBins );

Mat						splicemats(const vector<Mat>& mats, const string& flag);
void					slitmat(vector<Mat>& mats, Mat bigmat, const vector<int>& nums, const string& flag);

Mat						loadmat_from_matlab(const string& filename);
bool					writemat_for_matlab(const string& filename, Mat A);

Mat						randmat(int row, int col);
Mat						seqmat(int row, int col);
Mat						repmat(Mat A, int nrows, int ncols);
Mat						max(Mat A, int dim);
Mat						sum(Mat A, int dim);

vector<Mat>				read_from_images(const string& foldername, const string& suffix);
void					writevideo(const vector<Mat>& video, const string& video_filename);

// Texture feature:
Mat						WinnFilterBank(Mat labImg);
Mat						LBP(Mat c3image);

bool					write_sparse_matrix(const string& filename, const Sparse_Matrix* A);
Sparse_Matrix*			load_sparse_matrix(const string& filename); // NOTE: The return pointer must be manully deleted!!!


#pragma endregion utility_functions

#pragma region utility_classes
/************************************************************************/ 
/* PART II - ALL UTILS CLASSES.
*/
/************************************************************************/
class Timer // in seconds
{
public:
	Timer(){}
	~Timer(){}

	inline void										start_timer() {tstart = (double)clock()/CLOCKS_PER_SEC;}
	inline void										stop_timer() {tstop = (double)clock()/CLOCKS_PER_SEC;}
	inline double									get_time() {return tstop-tstart;}

	string											tell_now() 
	{
		time_t t = time(0); 
		char tmp[64]; 
		strftime( tmp, sizeof(tmp), "%H'%M'%S', %Y-%m-%d",localtime(&t) );
		return string(tmp);
	}

protected:
private:
	double tstart, tstop, ttime;
};

class BasicGMM
{
public:
	BasicGMM(){};
	~BasicGMM(){};

	void											Init();
	void											SetEM(const EM& em_model);

	int												N;
	Mat												Weights;
	Mat												Means;
	vector<Mat>										Sigmas;

	int												Mat_Type;
	// ================================================================================================
	// L_ik = log(weight_k) - 0.5 * log(|det(cov_k)|) - 0.5 *(x_i - mean_k)' cov_k^(-1) (x_i - mean_k)]
	// q = arg(max_k(L_ik))
	// probs_ik = exp(L_ik - L_iq) / (1 + sum_j!=q (exp(L_ij - L_iq))
	Mat												Prob(Mat feature) const;
	bool											Write(string filename);
	bool											Read(string filename);
	double											Pdf(Mat feature) const;

	void											CopyFrom(BasicGMM srcgmm);

private:

	Mat												LogWeights;
	Mat												DetSigmas;
	Mat												LogDetSigmas;
	vector<Mat>										InvSigmas;
};



#pragma endregion utility_classes

#endif