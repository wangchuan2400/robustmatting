#include "stdafx.h"
#include "Sparse_Matrix.h"
#include <fstream>

#ifdef WIN32
#	include <windows.h>
#	include <process.h>
#endif // WIN32

//void output_sparse_matrix( CFile* file, Sparse_Matrix* A )
//{
//	CString line;
//	int rows = A->rows();
//	int cols = A->cols();
//
//	line.Format( "rows: %d, cols %d \r\n\r\n", rows, cols );
//
//	if( NULL != file )
//		file->Write( line.GetBuffer(), line.GetLength() );
//	else
//		TRACE( line.GetString() );
//
//	for( int i = 0; i < rows; ++i )
//	{
//		for( int j = 0; j < cols; ++j )
//		{
//			if( j == cols - 1 )
//				line.Format( "%e; ", A->get_entry( i, j ) );
//			else
//				line.Format( "%e, ", A->get_entry( i, j ) );
//
//			if( NULL != file )
//				file->Write( line.GetBuffer(), line.GetLength() );
//			else
//				TRACE( line.GetString() );
//		}
//
//		line.Format( "\r\n\r\n" );
//
//		if( NULL != file )
//			file->Write( line.GetBuffer(), line.GetLength() );
//		else
//			TRACE( line.GetString() );
//	}
//}
//
//void output_sparse_matrix_nonzeroes( CFile* file, Sparse_Matrix* A )
//{
//	CString line;
//	int rows = A->rows();
//	int cols = A->cols();
//
//	line.Format( "rows: %d, cols %d \r\n\r\n", rows, cols );
//
//	if( NULL != file )
//		file->Write( line.GetBuffer(), line.GetLength() );
//	else
//		TRACE( line.GetString() );
//
//	int row = A->rows();
//	int col = A->cols();
//	int nonzero = A->get_nonzero();
//	int *rowind = A->get_rowind();
//	int *colptr = A->get_colptr();
//	double *values = A->get_values();
//
//	if( A->storage() == CCS)
//	{
//		int k = 0;
//		for( int i = 1; i < col + 1; i++ ) 
//		{
//			for( int j = 0; j < colptr[ i ] - colptr[ i - 1 ]; j++ ) 
//			{
//				//s<<rowind[k]<<" "<<i-1<<" "<< std::scientific<<values[k]<<"\n";
//				line.Format( "%d %d: %e\n", rowind[ k ], i - 1, values[ k ] );
//				file->Write( line.GetBuffer(), line.GetLength() );
//				k++;
//			}
//		}
//	}
//}

Sparse_Matrix* import_matrix_for_superlu_dist( std::string str_file_name )
{
	std::ifstream ifs( str_file_name.c_str() );
	if( !ifs )
	{
		//std::cout << "matrix file does not exist! " << std::endl;
		return NULL;
	}

	int n_row, n_col, nnz;
	ifs >> n_row >> n_col >> nnz;

	double* val = new double[ nnz ];
	int* rowind = new int[ nnz ];
	int* colptr = new int[ n_col + 1 ];

	for( int i = 0; i < nnz; ++i )
		ifs >> val[ i ];

	for( int i = 0; i < n_col + 1; ++i )
		ifs >> colptr[ i ];

	for( int i = 0; i < nnz; ++i )
		ifs >> rowind[ i ];

	Sparse_Matrix* A = new Sparse_Matrix( n_row, n_col );
	A->begin_fill_entry();


	for( int c = 0; c < n_col; 	++c )
	{
		for( int i = 0; i < colptr[ c+1 ] - colptr[ c ]; ++i )
		{
			int r = rowind[ colptr[c] + i ];
			double v = val[ colptr[c] + i ];
			A->set_entry( r, c, v );
		}
	}

	A->end_fill_entry();

	return A;
}

void export_for_superlu_dist( std::string str_file_name, Sparse_Matrix* A )
{
	assert( A->storage() == CCS );

	std::ofstream ofs( str_file_name.c_str() );

	//int_t *nrow, int_t *ncol, int_t *nonz,
	//double **nzval, int_t **rowind, int_t **colptr

	ofs << A->rows() << "\n";
	ofs << A->cols() << "\n";
	ofs << A->get_nonzero() << "\n";

	int nnz = A->get_nonzero();
	double* vals = A->get_values();
	for( int i = 0; i < nnz; ++i )
		ofs << std::scientific << vals[ i ] << "\n";

	int ncol = A->cols();
	int* colptr = A->get_colptr();
	for( int i = 0; i < ncol + 1; ++i )
		ofs << colptr[ i ] << "\n";

	int* rowind = A->get_rowind();
	for( int i = 0; i < nnz; ++i )
		ofs << rowind[ i ] << "\n";

}

void output_sparse_matrix_all_entries( std::string str_file_name, Sparse_Matrix* A )
{
	std::ofstream ofs( str_file_name.c_str() );

	int rows = A->rows();
	int cols = A->cols();

	char buffer[ 256 ];
	sprintf( buffer, "rows: %d, cols %d \r\n\r\n", rows, cols );

	ofs << buffer;

	for( int i = 0; i < rows; ++i )
	{
		for( int j = 0; j < cols; ++j )
		{
			if( j == cols - 1 )
				sprintf( buffer, "%.4e; ", A->get_entry( i, j ) );
			else
				sprintf( buffer, "%.4e, ", A->get_entry( i, j ) );

			ofs << buffer; 
		}

		ofs << "\r\n\r\n";
	}
}

Sparse_Matrix* convert_sym_matrix_to_nosym_storage( Sparse_Matrix* A )
{
	if( !A->issquare() )
		return NULL;

	if( !A->issymmetric() )
		return NULL;

	int row = A->rows();
	int col = A->cols();
	int nonzero = A->get_nonzero();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();

	Sparse_Matrix* ret = new Sparse_Matrix( row, col );

	ret->begin_fill_entry();

	if( CCS == A->storage() )
	{
		int k = 0;
		for( int i = 1; i < col + 1; i++ ) 
		{
			for( int j = 0; j < colptr[ i ] - colptr[ i - 1 ]; j++ ) 
			{
				ret->set_entry( rowind[ k ], i - 1, values[ k ] );
				ret->set_entry( i - 1, rowind[ k ], values[ k ] );
				k++;
			}
		}
	}
	else
	{
		assert( false );
	}

	ret->end_fill_entry();

	return ret;
}

std::ostream& operator<<(std::ostream &s, const Sparse_Matrix *A)
{
	s.precision(16);
	if (A == NULL) {
		s<<"the matrix is null !\n ";
	}

	int row = A->rows();
	int col = A->cols();
	int nonzero = A->get_nonzero();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();

	//s << row << " " << col << " " << nonzero << std::endl;
	s<<"row :"<<row<<" col :"<<col<<" Nonzero: "<<nonzero<<"\n\n";
	s<<"matrix --- (i, j, value)\n\n";

	SPARSE_STORAGE s_store = A->storage();
	if (s_store == CCS)
	{
		int k = 0;
		for (int i=1; i<col+1; i++) {
			for (int j=0; j<colptr[i]-colptr[i-1]; j++) {
				s<<rowind[k]<<" "<<i-1<<" "<< std::scientific<<values[k]<<"\n";
				k++;
			}
		}
	}
	else if (s_store == CRS)
	{
		int k = 0;
		for (int i=1; i<row+1; i++) {
			for (int j=0; j<rowind[i]-rowind[i-1]; j++) {
				s<<i-1<<" "<<colptr[k]<<" "<< std::scientific<<values[k]<<"\n";
				k++;
			}
		}
	}
	else if (s_store == TRIPLE)
	{
		for (int k = 0; k < nonzero; k++)
		{
			s << rowind[k] <<" " << colptr[k] << " " << std::scientific<<values[k]<<"\n";
		}
	}


	//double *B = A->get_B();
	//s<<"\n\n B -- (i, value) \n\n";
	//for (int i=0; i<A->get_num_rhs()*row; i++) {
	//	s<<i<<" "<< std::scientific<<B[i]<<"\n";
	//}

	//double *solution = A->get_solution();
	//s<<"\n\n Solution -- (i, value) \n\n";
	//for (int i=0; i<A->get_num_rhs()*col; i++) {
	//	s<<i<<" "<< std::scientific<<solution[i]<<"\n";
	//}
	return s;
}
//////////////////////////////////////////////////////////////////////////
void multiply(const Sparse_Matrix *A, const double *X, double *Y)
{
	int row = A->rows();
	int col = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();
	int nonzero = A->get_nonzero();
	memset(Y, 0, sizeof(double)*row);

	SPARSE_STORAGE s_store = A->storage();

	bool halfstore = A->issym_store_upper_or_lower();

	if (s_store == CCS)
	{
		int k = 0;
		for (int i=1; i<col+1; i++) 
		{
			for (int j=0; j<colptr[i]-colptr[i-1]; j++)
			{
				Y[rowind[k]] += values[k]*X[i-1];
				if (halfstore && rowind[k] != i-1)
				{
					Y[i-1] += values[k]*X[rowind[k]];
				}
				k++;
			}
		}
	}
	else if (s_store == CRS)
	{
		int k = 0;
		for (int i=1; i<row+1; i++) 
		{
			for (int j=0; j<rowind[i]-rowind[i-1]; j++)
			{
				Y[i-1] += values[k]*X[colptr[k]];
				if (halfstore && colptr[k] != i-1)
				{
					Y[colptr[k]] += values[k]*X[i-1];
				}
				k++;
			}
		}
	}
	else if (s_store == TRIPLE)
	{
		for (int i = 0; i < nonzero; i++)
		{
			Y[rowind[i]] += values[i] * X[colptr[i]];
			if (halfstore && rowind[i] != colptr[i])
			{
				Y[colptr[i]] += values[i] * X[rowind[i]];
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void transpose_multiply(const Sparse_Matrix *A, const double *X, double *Y)
{
	int row = A->rows();
	int col = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();
	int nonzero = A->get_nonzero();
	memset(Y, 0, sizeof(double)*col);

	SPARSE_STORAGE s_store = A->storage();

	bool halfstore = A->issym_store_upper_or_lower();

	if (s_store == CCS)
	{
		int k = 0;
		for (int i=1; i<col+1; i++) 
		{
			for (int j=0; j<colptr[i]-colptr[i-1]; j++)
			{
				Y[i-1] += values[k]*X[rowind[k]];
				if (halfstore && rowind[k] != i-1)
				{
					Y[rowind[k]] += values[k]*X[i-1];
				}
				k++;
			}
		}
	}
	else if (s_store == CRS)
	{
		int k = 0;
		for (int i=1; i<row+1; i++) 
		{
			for (int j=0; j<rowind[i]-rowind[i-1]; j++)
			{
				Y[colptr[k]] += values[k]*X[i-1];
				if (halfstore && colptr[k] != i-1)
				{
					Y[i-1] += values[k]*X[colptr[k]];
				}
				k++;
			}
		}
	}
	else if (s_store == TRIPLE)
	{
		for (int i = 0; i < nonzero; i++)
		{
			Y[colptr[i]] += values[i] * X[rowind[i]];
			if (halfstore && rowind[i] != colptr[i])
			{
				Y[rowind[i]] += values[i] * X[colptr[i]];
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void transpose_self_multiply(const Sparse_Matrix *A, const double *X, double *Y, double *tmp)
{
	multiply(A, X, tmp);
	transpose_multiply(A, tmp, Y);
}
//////////////////////////////////////////////////////////////////////////
Sparse_Matrix* convert_sparse_matrix_storage(const Sparse_Matrix *A, SPARSE_STORAGE store, SYMMETRIC_STATE sym)
{
	int row = A->rows();
	int col = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();
	int nonzero = A->get_nonzero();

	Sparse_Matrix* new_mat = new Sparse_Matrix(row, col, A->issymmetric()?sym:NOSYM, A->isspd(), store, A->get_num_rhs());

	SPARSE_STORAGE s_store = A->storage();

	new_mat->begin_fill_entry();

	if (s_store == CCS)
	{
		int k = 0;
		for (int i=1; i<col+1; i++) {
			for (int j=0; j<colptr[i]-colptr[i-1]; j++) {
				new_mat->fill_entry(rowind[k], i-1, values[k]);
				k++;
			}
		}
	}
	else if (s_store == CRS)
	{
		int k = 0;
		for (int i=1; i<row+1; i++) {
			for (int j=0; j<rowind[i]-rowind[i-1]; j++) {
				new_mat->fill_entry(i-1, colptr[k], values[k]);
				k++;
			}
		}
	}
	else if (s_store == TRIPLE)
	{
		for (int k = 0; k < nonzero; k++)
		{
			new_mat->fill_entry(rowind[k], colptr[k], values[k]);
		}
	}

	new_mat->end_fill_entry();

	memcpy(new_mat->get_B(), A->get_B(), sizeof(double)* A->get_num_rhs()*row);

	return new_mat;
}
//////////////////////////////////////////////////////////////////////////
//! matrix transposition
Sparse_Matrix* transpose(Sparse_Matrix* A, SPARSE_STORAGE store, SYMMETRIC_STATE sym)
{
	int nrows = A->rows();
	int ncols = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();
	int nonzero = A->get_nonzero();
	SPARSE_STORAGE s_store = A->storage();

	Sparse_Matrix* tr_A = new Sparse_Matrix(ncols, nrows, A->symmetric_state()?sym:NOSYM, A->isspd(), 
		store, A->get_num_rhs());

	tr_A->begin_fill_entry();
	int k = 0;
	if (s_store == CCS)
	{
		for (int i=1; i<ncols+1; i++) {
			for (int j=0; j<colptr[i]-colptr[i-1]; j++) {
				tr_A->fill_entry(i-1, rowind[k], values[k]);
				k++;
			}
		}
	}
	else if (s_store == CRS)
	{
		for (int i=1; i<nrows+1; i++) {
			for (int j=0; j<rowind[i]-rowind[i-1]; j++) {
				tr_A->fill_entry(colptr[k], i-1, values[k]);
				k++;
			}
		}
	}
	else if (s_store == TRIPLE)
	{
		for (int i = 0; i < nonzero; i++)
		{
			tr_A->fill_entry(colptr[i], rowind[i], values[i]);
		}
	}

	tr_A->end_fill_entry();

	return tr_A;
}
//////////////////////////////////////////////////////////////////////////
struct MT_TRAN_MULT_DATA
{
	Sparse_Matrix*				mat_;
	int							col_begin_, col_end_;
	std::vector< int >			vec_row_ids_;
	std::vector< int >			vec_col_ids_;
	std::vector< double >		vec_values_;
};

static void TransposeTimesSelf_thread( void* p_mt_data )
{
#ifdef WIN32

	MT_TRAN_MULT_DATA* data = (MT_TRAN_MULT_DATA*)p_mt_data;

	int cs_col = data->mat_->cols();
	int *cs_rowind = data->mat_->get_rowind();
	int *cs_colptr = data->mat_->get_colptr();
	double *cs_values = data->mat_->get_values();

	for( int i = data->col_begin_; i < data->col_end_; ++i )
	{
		for( int j = 0; j <= i; ++j )
		{
			double sum  = 0;

			//col i * col j

			int kii = cs_colptr[i];
			int kjj = cs_colptr[j];
			for (int ii=0, jj = 0; ii<cs_colptr[i+1]-cs_colptr[i] && jj < cs_colptr[j+1]-cs_colptr[j];) {

				if (cs_rowind[kii] == cs_rowind[kjj])
				{
					sum += cs_values[kii]*cs_values[kjj];
					kii++;
					kjj++;
					ii++;
					jj++;
				}
				else if(cs_rowind[kii] < cs_rowind[kjj])
				{
					kii++;
					ii++;
				}
				else 
				{
					kjj++;
					jj++;
				}
			}

			if (sum != 0)
			{
				//A->fill_entry(i, j, sum);
				data->vec_row_ids_.push_back( i );
				data->vec_col_ids_.push_back( j );
				data->vec_values_.push_back( sum );
			}
		}
	}

	_endthread();

#endif // WIN32
}

Sparse_Matrix* TransposeTimesSelf_mt( Sparse_Matrix* mat, bool positive, SPARSE_STORAGE store, SYMMETRIC_STATE sym, 
									  Sparse_Matrix* A, int n_thread, double f_scalar )
{
#ifdef WIN32

	Sparse_Matrix* CS = mat; 

	if (mat->storage() != CCS || (mat->symmetric_state() != NOSYM && mat->symmetric_state() != SYM_BOTH) ) 
	{
		CS = convert_sparse_matrix_storage(mat, CCS, SYM_BOTH);
	}

	int cs_col = CS->cols();
	int *cs_rowind = CS->get_rowind();
	int *cs_colptr = CS->get_colptr();
	double *cs_values = CS->get_values();

	bool b_input_A = NULL == A ? false : true;

	if( NULL == A )
		A = new Sparse_Matrix(cs_col, cs_col, sym==NOSYM?SYM_LOWER:sym, positive, store, mat->get_num_rhs());

	if( !b_input_A )
		A->begin_fill_entry();

	MT_TRAN_MULT_DATA* data = new MT_TRAN_MULT_DATA[ n_thread ];

	for( int i = 0; i < n_thread; ++i )
	{
		data[ i ].col_begin_ = int( sqrt( double(i) ) * cs_col / sqrt( double( n_thread ) ) );
		data[ i ].col_end_ = ( i != n_thread-1 ) ? int( sqrt( double( i + 1 ) ) * cs_col / sqrt( double( n_thread ) ) ) : cs_col;
		data[ i ].mat_ = CS;
		data[ i ].vec_col_ids_.reserve( 128 * 1024 );
		data[ i ].vec_row_ids_.reserve( 128 * 1024 );
		data[ i ].vec_values_.reserve( 128 * 1024 );
	}

	HANDLE* thread_handles = new HANDLE[ n_thread ];
	for( int i = 0; i < n_thread; ++i )
		thread_handles[ i ] = (HANDLE*)_beginthread( TransposeTimesSelf_thread, 0, (void*)(data + i) );

	for( int i = 0; i < n_thread; ++i )
		WaitForSingleObject( thread_handles[i], INFINITE );

	for( int i = 0; i < n_thread; ++i )
	{
		std::vector< int >::const_iterator c_ite = data[ i ].vec_col_ids_.begin();
		std::vector< int >::const_iterator r_ite = data[ i ].vec_row_ids_.begin();
		std::vector< double >::const_iterator v_ite = data[ i ].vec_values_.begin();

		for( ; c_ite != data[ i ].vec_col_ids_.end(); ++c_ite, ++r_ite, ++v_ite )
			A->fill_entry( *r_ite, *c_ite, (*v_ite) * f_scalar );	
	}

	if( !b_input_A )
		A->end_fill_entry();

	if( CS != mat )
		delete CS;

	delete [] data;
	delete [] thread_handles;

	return A;

#else // WIN32
	assert( false );
	return NULL;
#endif // WIN32
}

struct MT_KKT_MULT_DATA
{
	Sparse_Matrix				*L_, *B_, *M_;
	int							col_begin_, col_end_;

	std::vector< int >			vec_row_ids_;
	std::vector< int >			vec_col_ids_;
	std::vector< double >		vec_values_;
};

static void multiply_for_kkt_thread( void* p_mt_data )
{
#ifdef WIN32

	// M + f_scalar * L^T * B + f_scalar * B^T * L

	MT_KKT_MULT_DATA* data = (MT_KKT_MULT_DATA*)p_mt_data;

	int L_col = data->L_->cols();
	int* L_rowind = data->L_->get_rowind();
	int* L_colptr = data->L_->get_colptr();
	double* L_values = data->L_->get_values();

	int B_col = data->B_->cols();
	int* B_rowind = data->B_->get_rowind();
	int* B_colptr = data->B_->get_colptr();
	double* B_values = data->B_->get_values();

	for( int i = data->col_begin_; i < data->col_end_; ++i )
	{
		for( int j = 0; j < B_col; ++j )
		{
			double sum  = 0;

			//col i of L * col j of B

			int L_k = L_colptr[i];
			int B_k = B_colptr[j];
			for( int L_ii =0, B_ii = 0; L_ii < L_colptr[i+1]-L_colptr[i] && B_ii < B_colptr[j+1]-B_colptr[j]; ) 
			{
				if( L_rowind[ L_k ] == B_rowind[ B_k ] )
				{
					sum += L_values[ L_k ] * B_values[ B_k ];
					++L_k;
					++B_k;
					++L_ii;
					++B_ii;
				}
				else if( L_rowind[ L_k ] < B_rowind[ B_k ] )
				{
					++L_k;
					++L_ii;
				}
				else 
				{
					++B_k;
					++B_ii;
				}
			}

			if( sum != 0 )
			{
				data->vec_row_ids_.push_back( i );
				data->vec_col_ids_.push_back( j );

				if( i == j )
					sum *= 2;

				data->vec_values_.push_back( sum );
			}
		}
	}

#else // WIN32

	assert( false );

#endif // WIN32

}

void multiply_for_kkt_mt( Sparse_Matrix* L, Sparse_Matrix* B, Sparse_Matrix* M, int n_thread, double f_scalar )
{
#ifdef WIN32

	// M + f_scalar * L^T * B + f_scalar * B^T * L

	L = convert_sparse_matrix_storage( L, CCS, SYM_BOTH);
	B = convert_sparse_matrix_storage( B, CCS, SYM_BOTH );

	MT_KKT_MULT_DATA* data = new MT_KKT_MULT_DATA[ n_thread ];

	int col = L->cols();
	int n_step = col / n_thread;
	for( int i = 0; i < n_thread; ++i )
	{
		data[ i ].col_begin_ = i * n_step;
		data[ i ].col_end_ = ( i != n_thread-1 ) ? ( i + 1 ) * n_step : col;
		data[ i ].L_ = L;
		data[ i ].B_ = B;
		data[ i ].M_ = M;
	}

	HANDLE* thread_handles = new HANDLE[ n_thread ];
	for( int i = 0; i < n_thread; ++i )
		thread_handles[ i ] = (HANDLE*)_beginthread( multiply_for_kkt_thread, 0, (void*)(data + i) );

	for( int i = 0; i < n_thread; ++i )
		WaitForSingleObject( thread_handles[i], INFINITE );

	for( int i = 0; i < n_thread; ++i )
	{
		std::vector< int >::const_iterator c_ite = data[ i ].vec_col_ids_.begin();
		std::vector< int >::const_iterator r_ite = data[ i ].vec_row_ids_.begin();
		std::vector< double >::const_iterator v_ite = data[ i ].vec_values_.begin();

		for( ; c_ite != data[ i ].vec_col_ids_.end(); ++c_ite, ++r_ite, ++v_ite )
			M->fill_entry( *r_ite, *c_ite, (*v_ite) * f_scalar );
	}

	delete L;
	delete B;
	delete [] thread_handles;
	delete [] data;

#else // WIN32
	assert( false );
#endif // WIN32
}

//////////////////////////////////////////////////////////////////////////
Sparse_Matrix* TransposeTimesSelf( Sparse_Matrix* mat, bool positive, SPARSE_STORAGE store, SYMMETRIC_STATE sym, Sparse_Matrix* A /*= NULL*/ )
{
	Sparse_Matrix* CS = mat; 

	if (mat->storage() != CCS || (mat->symmetric_state() != NOSYM && mat->symmetric_state() != SYM_BOTH) ) 
	{
		CS = convert_sparse_matrix_storage(mat, CCS, SYM_BOTH);
	}

	int cs_col = CS->cols();
	int *cs_rowind = CS->get_rowind();
	int *cs_colptr = CS->get_colptr();
	double *cs_values = CS->get_values();

	bool b_input_A = NULL == A ? false : true;

	if( NULL == A )
		A = new Sparse_Matrix(cs_col, cs_col, sym==NOSYM?SYM_LOWER:sym, positive, store, mat->get_num_rhs());
	else
		assert( SYM_LOWER == A->symmetric_state() && CCS == A->storage() );

	if( !b_input_A )
		A->begin_fill_entry();

	for (int i = 0; i < cs_col; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			double sum  = 0;

			//col i * col j

			int kii = cs_colptr[i];
			int kjj = cs_colptr[j];
			for (int ii=0, jj = 0; ii<cs_colptr[i+1]-cs_colptr[i] && jj < cs_colptr[j+1]-cs_colptr[j];) {

				if (cs_rowind[kii] == cs_rowind[kjj])
				{
					sum += cs_values[kii]*cs_values[kjj];
					kii++;
					kjj++;
					ii++;
					jj++;
				}
				else if(cs_rowind[kii] < cs_rowind[kjj])
				{
					kii++;
					ii++;
				}
				else 
				{
					kjj++;
					jj++;
				}
			}

			if (sum != 0)
			{
				A->fill_entry(i, j, sum);
			}
		}
	}

	if( !b_input_A )
		A->end_fill_entry();

	if (CS != mat)
	{
		delete CS;
	}

	return A;
}
//////////////////////////////////////////////////////////////////////////
Sparse_Matrix* SelfTimesTranspose(Sparse_Matrix* mat, bool positive,  SPARSE_STORAGE store, SYMMETRIC_STATE sym)
{
	Sparse_Matrix *trans_mat = transpose(mat, CCS, SYM_BOTH);
	Sparse_Matrix *M = TransposeTimesSelf(trans_mat, positive, store, sym);
	delete trans_mat;
	return M;
}

Sparse_Matrix* get_copy( const Sparse_Matrix* A )
{
	if( A->storage() != CCS )
		return NULL;

	int col = A->cols();
	int* colptr = A->get_colptr();
	int* rowind = A->get_rowind();
	double* values = A->get_values();

	Sparse_Matrix* B = new Sparse_Matrix( A->rows(), A->cols(), A->symmetric_state(), A->isspd(), CCS );

	B->begin_fill_entry();

	int k = 0;
	for( int i = 1; i < col + 1; i++ ) 
	{
		for( int j = 0; j < colptr[ i ] - colptr[ i - 1 ]; j++ ) 
		{
			B->fill_entry( rowind[ k ], i - 1, values[ k ] );
			k++;
		}
	}

	B->end_fill_entry();

	return B;
}

Sparse_Matrix* add( const Sparse_Matrix* A, const Sparse_Matrix* B )
{
	if( A->rows() != B->rows() )
		return NULL;
	if( A->cols() != B->cols() )
		return NULL;

	bool new_a = false, new_b = false;

	bool bsym = A->issymmetric() && B->issymmetric();
	bool bspd = A->isspd() && B->isspd();

	if( bsym )
	{
		if( A->storage() != CCS || 
			( A->symmetric_state() != NOSYM && A->symmetric_state() != SYM_LOWER ) ) 
		{
			A = convert_sparse_matrix_storage( A, CCS, SYM_LOWER );
			new_a = true;
		}

		if( B->storage() != CCS || 
			( B->symmetric_state() != NOSYM && B->symmetric_state() != SYM_LOWER ) ) 
		{
			B = convert_sparse_matrix_storage( A, CCS, SYM_LOWER );
			new_b = true;
		}
	}
	else
	{
		if( A->issymmetric() || A->storage() != CCS )
		{
			A = convert_sparse_matrix_storage( A, CCS, NOSYM );
			new_a = true;
		}

		if( B->issymmetric() || B->storage() != CCS )
		{
			B = convert_sparse_matrix_storage( A, CCS, NOSYM );
			new_b = true;
		}
	}

	Sparse_Matrix* ret = new Sparse_Matrix( A->rows(), A->cols(), bsym?SYM_LOWER:NOSYM, bspd );

	int row_a = A->rows();
	int col_a = A->cols();
	int nonzero_a = A->get_nonzero();
	int *rowind_a = A->get_rowind();
	int *colptr_a = A->get_colptr();
	double *values_a = A->get_values();

	SPARSE_STORAGE s_store_a = A->storage();

	int row_b = B->rows();
	int col_b = B->cols();
	int nonzero_b = B->get_nonzero();
	int *rowind_b = B->get_rowind();
	int *colptr_b = B->get_colptr();
	double *values_b = B->get_values();

	SPARSE_STORAGE s_store_b = B->storage();

	ret->begin_fill_entry();

	int k = 0;
	for( int i = 1; i < col_a + 1; i++ ) 
	{
		for( int j = 0; j < colptr_a[ i ] - colptr_a[ i - 1 ]; j++ ) 
		{
			ret->fill_entry( rowind_a[ k ], i - 1, values_a[ k ] );
			k++;
		}
	}

	k = 0;
	for( int i = 1; i < col_b + 1; i++ ) 
	{
		for( int j = 0; j < colptr_b[ i ] - colptr_b[ i - 1 ]; j++ ) 
		{
			ret->fill_entry( rowind_b[ k ], i - 1, values_b[ k ] );
			k++;
		}
	}
	
	ret->end_fill_entry();

	if( new_a )
		delete A;
	if( new_b )
		delete B;

	return ret;
}

//////////////////////////////////////////////////////////////////////////
void multiply( Sparse_Matrix* A, double f )
{
	double *values = A->get_values();
	int nonzero = A->get_nonzero();

	for( int i = 0; i < nonzero; ++i )
		values[ i ] *= f;



	//int nonzero = A->get_nonzero();
	//int *rowind = A->get_rowind();
	//int *colptr = A->get_colptr();

	//SPARSE_STORAGE s_store = A->storage();
	//if (s_store == CCS)
	//{
	//	int k = 0;
	//	for (int i=1; i<col+1; i++) {
	//		for (int j=0; j<colptr[i]-colptr[i-1]; j++) {
	//			s<<rowind[k]<<" "<<i-1<<" "<< std::scientific<<values[k]<<"\n";
	//			k++;
	//		}
	//	}
	//}
	//else if (s_store == CRS)
	//{
	//	int k = 0;
	//	for (int i=1; i<row+1; i++) {
	//		for (int j=0; j<rowind[i]-rowind[i-1]; j++) {
	//			s<<i-1<<" "<<colptr[k]<<" "<< std::scientific<<values[k]<<"\n";
	//			k++;
	//		}
	//	}
	//}
	//else if (s_store == TRIPLE)
	//{
	//	for (int k = 0; k < nonzero; k++)
	//	{
	//		s << rowind[k] <<" " << colptr[k] << " " << std::scientific<<values[k]<<"\n";
	//	}
	//}

}

//////////////////////////////////////////////////////////////////////////
Sparse_Vector::Sparse_Vector( int n )
{
	n_ = n;
	values_ = new double[ n ];
	//memset( values_, 0, sizeof( double ) * n );
}

Sparse_Vector::Sparse_Vector( double* values, int n )
{
	n_ = n;
	values_ = new double[ n ];

	for( int i = 0; i < n; ++i )
	{
		if( values[ i ] != 0.0 )
		{
			nnz_indices_.insert( i );
			values_[ i ] = values[ i ];
		}
	}
}

Sparse_Vector::Sparse_Vector( const Sparse_Matrix* A, int index, bool b_col/* = true */)
{
	assert( true == b_col );
	assert( A->storage() == CCS );

	n_ = A->rows();
	values_ = new double[ n_ ];

	int*	rowind = A->get_rowind();
	int*	colptr = A->get_colptr();
	double*	values = A->get_values();

	int		colptr_next = colptr[ index + 1 ];

	int		id;
	for( int k = colptr[ index ]; k < colptr_next; ++k )
	{
		id = rowind[ k ];
		nnz_indices_.insert( id ); 
		values_[ id ] = values[ k ];;
	}
}

Sparse_Vector::~Sparse_Vector()
{
	delete [] values_;
}

double Sparse_Vector::dot_product( const double* v ) const
{
	double sum = 0;
	int id;

	std::set<int>::const_iterator ite = nnz_indices_.begin();
	for( ; ite != nnz_indices_.end(); ++ite )
	{
		id = *ite;
		sum += values_[ id ] * v[ id ];
	}
	return sum;
}

//double Sparse_Vector::dot_product( const Sparse_Vector* v ) const
//{
//	assert( n_ == v->n_ );
//
//	double sum = 0;
//	int id;
//
//	std::set<int>::const_iterator ite = nnz_indices_.begin();
//	for( ; ite != nnz_indices_.end(); ++ite )
//	{
//		id = *ite;
//
//		if( v->nnz_indices_.find( id ) != v->nnz_indices_.end() )
//			sum += values_[ id ] * v->values_[ id ];
//	}
//	return sum;
//}

void Sparse_Vector::multiply( double x ) const
{
	std::set<int>::const_iterator ite = nnz_indices_.begin();
	for( ; ite != nnz_indices_.end(); ++ite )
		values_[ *ite ] *= x;
}

//Sparse_Vector* Sparse_Vector::add( const Sparse_Vector* v ) const
//{
//	assert( n_ == v->n_ );
//
//	Sparse_Vector* ret = new Sparse_Vector( n_ );
//	ret->nnz_indices_ = nnz_indices_;
//
//	std::set<int>::const_iterator ite = nnz_indices_.begin();
//	int id;
//	for( ; ite != nnz_indices_.end(); ++ite )
//	{
//		id = *ite;
//		ret->values_[ id  ] = values_[ id ];
//	}
//
//	ret->self_add( v );
//	return ret;
//}

void Sparse_Vector::self_add( const Sparse_Vector* v )
{
	assert( n_ == v->n_ );

	std::set<int>::const_iterator ite = v->nnz_indices_.begin();
	for( ; ite != v->nnz_indices_.end(); ++ite )
	{
		int id = *ite;
		fill_entry( id, v->values_[ id ] );
	}
}