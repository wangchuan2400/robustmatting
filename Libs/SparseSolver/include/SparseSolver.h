// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SPARSESOLVER_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SPARSESOLVER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SPARSESOLVER_EXPORTS
#define SPARSESOLVER_API __declspec(dllexport)
#else
#define SPARSESOLVER_API __declspec(dllimport)
#endif

#define					SAFE_DELETE(ptr) if(ptr){delete ptr; ptr=NULL;}
#define					SAFE_DELETE_ARRAY(ptr) if(ptr){delete [] ptr; ptr=NULL;}

#include "sparse_matrix.h"

namespace cwg
{
	class SPARSESOLVER_API SparseSolver 
	{
	public:
		SparseSolver(int nukns);
		~SparseSolver();

		void												Init();
		void												Run(int iter=100);

	Sparse_Matrix*											A;
	double*													b;
	double*													x;
	int														nUkns;
	};
}
// This class is exported from the SparseSolver.dll

