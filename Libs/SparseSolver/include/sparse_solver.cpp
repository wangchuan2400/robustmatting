#include "Sparse_Solver.h"
#include <iostream>

//////////////////////////////////////////////////////////////////////////
//! TAUCS: Sparse Linear Solver
/*!
*	<A HREF= "http://www.tau.ac.il/~stoledo/taucs/"> TAUCS 2.2 </A>
*/
#ifdef USE_TAUCS
extern "C"
{
//#include "..\taucs\taucs.h"
#include <Geex/third_party/numeric_stuff/taucs/taucs.h>
}
//#pragma comment (lib, "vcf2c.lib") // for fortran to c
//#pragma comment (lib, "libmetis.lib") // for metis
//#pragma comment (lib, "libtaucs.lib") 
#endif

//////////////////////////////////////////////////////////////////////////
//! UMFPACK: Sparse Linear Solver
/*!
*	<A HREF= "http://www.cise.ufl.edu/research/sparse/umfpack/"> UMFPACK 5.0 </A>
*/
#ifdef USE_UMFPACK
extern "C"
{
#include "../umfpack/umfpack.h"
}
#pragma comment (lib, "amd.lib")
#pragma comment (lib, "umfpack.lib") 
#endif
//////////////////////////////////////////////////////////////////////////
//! ARPACK: a collection of Fortran77 subroutines designed to solve large scale eigenvalue problems

/*!
*	<A HREF= "http://www.cise.ufl.edu/research/sparse/umfpack/"> ARPACK </A>
*/
#ifdef USE_ARPACK
// #pragma comment (lib, "arpack.lib")
//#include <Geex/third_party/numeric_stuff/arpack/arpack.h>
//////////////////////////////////////////////////////////////////////////
#endif

namespace Geex
{

//////////////////////////////////////////////////////////////////////////
#ifdef USE_TAUCS
bool solve_by_TAUCS(Sparse_Matrix *m_sparse_matrix) 
{
	if (m_sparse_matrix==NULL)
	{
		return false;
	}

	Sparse_Matrix *A = m_sparse_matrix;
	if (m_sparse_matrix->storage() != CCS)
	{
		A = convert_sparse_matrix_storage(m_sparse_matrix, CCS, m_sparse_matrix->symmetric_state());
	}

	bool spd = A->isspd();
	int row = A->rows();
	int col = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();
	int m_num_column_of_RHS = A->get_num_rhs();
	double *B = m_sparse_matrix->get_B();
	double *solution = m_sparse_matrix->get_solution();

	taucs_ccs_matrix mat; 
	mat.colptr = colptr;
	mat.rowind = rowind;
	mat.m = row;
	mat.n = col;
	mat.values.d = values;

	if (spd) {
		mat.flags = TAUCS_DOUBLE|TAUCS_LOWER|TAUCS_SYMMETRIC;

		char* metis[] = {"taucs.factor.LLT=true", NULL}; 
		int res = taucs_linsolve(&mat, NULL, m_num_column_of_RHS, solution, B, metis, NULL);

		if (A != m_sparse_matrix)
		{
			delete A;
		}
		return (res==TAUCS_SUCCESS);
	}
	else
	{

		mat.flags = TAUCS_DOUBLE;

		taucs_io_handle* LU;
		char fname[]="taucs";
		int* perm=NULL;
		int* invperm=NULL;
		taucs_ccs_order(&mat, &perm, &invperm, "colamd");	
		LU = taucs_io_create_multifile(fname);


		//danger! if the matrix is singular
		taucs_ooc_factor_lu(&mat, perm, LU, taucs_available_memory_size()); // taucs_available_memory_size()*factor is ok, 
		//note in debug mode, the program will hangle
		for (int i = 0; i < m_num_column_of_RHS; i++)
		{
			taucs_ooc_solve_lu(LU, &solution[i*row], &B[i*row]);
		}

		if (perm) {
			free(perm);
		}
		if (invperm) {
			free(invperm);
		}

		taucs_io_delete(LU);

		if (A != m_sparse_matrix)
		{
			delete A;
		}
		return true;
	}
}

#endif

#ifdef USE_UMFPACK
bool solve_by_UMFPACK(Sparse_Matrix *m_sparse_matrix) 
{
	if (m_sparse_matrix==NULL)
	{
		return false;
	}

	Sparse_Matrix *A = m_sparse_matrix;
	if (m_sparse_matrix->storage() != CCS)
	{
		A = convert_sparse_matrix_storage(m_sparse_matrix, CCS, m_sparse_matrix->symmetric_state());
	}

	int row = A->rows();
	int col = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();
	int m_num_column_of_RHS = A->get_num_rhs();
	double *B = m_sparse_matrix->get_B();
	double *solution = m_sparse_matrix->get_solution();

	double Info [UMFPACK_INFO];
	double *null = (double *)NULL;
	void *Symbolic, *Numeric;

	(void)umfpack_di_symbolic(row, col, colptr, rowind, values, &Symbolic, null, null );
	int status = umfpack_di_numeric(colptr, rowind, values, Symbolic, &Numeric, null, Info );
	if (status<0 || (int)Info[UMFPACK_STATUS]==UMFPACK_WARNING_singular_matrix) {
		umfpack_di_free_symbolic(&Symbolic);
		umfpack_di_free_numeric(&Numeric);
		if (A != m_sparse_matrix)
		{
			delete A;
		}
		return false;
	}
	umfpack_di_free_symbolic(&Symbolic);

	for (int i = 0; i < m_num_column_of_RHS; i++)
	{
		(void)umfpack_di_solve(UMFPACK_A, colptr, rowind, values, &solution[i*row], &B[i*row], Numeric, null, null);
	}

	umfpack_di_free_numeric(&Numeric);

	if (A != m_sparse_matrix)
	{
		delete A;
	}

	return true;
}

bool inverse_power_method_by_UMFPACK( Sparse_Matrix* m_sparse_matrix, double* target_eigen_vec )
{
	if( NULL == m_sparse_matrix )
	{
		return false;
	}

	Sparse_Matrix *A = m_sparse_matrix;
	if( CCS != m_sparse_matrix->storage() )
	{
		A = convert_sparse_matrix_storage( m_sparse_matrix, CCS, m_sparse_matrix->symmetric_state()	);
	}

	Sparse_Matrix* sym_mat = A;
	if( A->issymmetric() )
	{
		A = convert_sym_matrix_to_nosym_storage( A );
	}

	if( sym_mat != A && sym_mat != m_sparse_matrix )
		delete sym_mat;

	int row = A->rows();
	int col = A->cols();
	int *rowind = A->get_rowind();
	int *colptr = A->get_colptr();
	double *values = A->get_values();

	double Info[ UMFPACK_INFO ];
	double* null = (double*)NULL;
	void* Symbolic;
	void *Numeric;

	(void)umfpack_di_symbolic( row, col, colptr, rowind, values, &Symbolic, null, null );

	int status = umfpack_di_numeric( colptr, rowind, values, Symbolic, &Numeric, null, Info );
	
	if( status < 0 || UMFPACK_WARNING_singular_matrix == (int)Info[ UMFPACK_STATUS ] ) 
	{
		umfpack_di_free_symbolic( &Symbolic );
		umfpack_di_free_numeric( &Numeric );
		if( A != m_sparse_matrix )
		{
			delete A;
		}
		return false;
	}
	
	umfpack_di_free_symbolic( &Symbolic );

	int n_step = 0;
	double* eigen_vec = new double[ row ];
	double* eigen_vec_next = new double[ row ];

	for( int i = 0; i < row; ++i )
		eigen_vec[ i ] = sqrt( i + 1.0 );

	double diff = 0.0;
	while( true )
	{
		( void ) umfpack_di_solve( UMFPACK_A, colptr, rowind, values, eigen_vec_next, eigen_vec, Numeric, null, null );
		
		double* tmp = eigen_vec;
		eigen_vec = eigen_vec_next;
		eigen_vec_next = tmp;

		double sqrd_length = 0.0;
		for( int i = 0; i < row; ++i )
			sqrd_length  += eigen_vec[ i ] * eigen_vec[ i ];
		
		double len = sqrt( sqrd_length );

		for( int i = 0; i < row; ++i )
			eigen_vec[ i ] /= len;

		diff = 0.0;
		for( int i = 0; i < row; ++i )
			diff += ( eigen_vec[ i ] - eigen_vec_next[ i ] ) * ( eigen_vec[ i ] - eigen_vec_next[ i ] );

		if( diff < 1e-10 )
			break;

		if( n_step >= 2000 ) // maximum iterations
			break;

		++n_step;
	}

	memcpy( target_eigen_vec, eigen_vec, sizeof( double ) * row );

//#ifdef _MY_DEBUG
	//TRACE( "**** UMFPACK inverse power method: step: %d, diff: %f ****\n", n_step, diff );
	//vector< double > dbg1, dbg2;
	//multiply( A, eigen_vec, eigen_vec_next );
	//for( int i = 0; i < row; ++i )
	//{
	//	dbg1.push_back( eigen_vec[ i ] );
	//	dbg2.push_back( eigen_vec_next[ i ] );
	//	TRACE( "UMFPACK eigval: %f\n", eigen_vec[ i ] / eigen_vec_next[ i ] );
	//}
//#endif // _MY_DEBUG

	umfpack_di_free_numeric( &Numeric );

	if( A != m_sparse_matrix )
	{
		delete A;
	}

	delete [] eigen_vec;
	delete [] eigen_vec_next;

	if( n_step >= 2000 )
		return false;

	return true;
}

#endif

#ifdef USE_ARPACK

bool solve_SVD_by_ARPACK(Sparse_Matrix *A, int num_of_eigenvalue, int eigentype, 
						 double *eigenvalues, double *eigenvectors)
{
	int n = A->cols();
	int nev = num_of_eigenvalue;
	int ido = 0;
	char bmat[2] = "I";
	double tol = 0.0;
	double *resid = new double[n];
	int ncv = 4*nev;
	if (ncv>n) ncv = n;
	int ldv = n;
	double *v = new double[ldv*ncv];;
	int iparam[11]; /* An array used to pass information to the routines
					about their functional modes. */
	iparam[0] = 1; // Specifies the shift strategy (1->exact)

	iparam[2] = 3*n; // Maximum number of iterations

	iparam[6] = 1;/* Sets the mode of dsaupd.
				  1 is exact shifting, 
				  2 is user-supplied shifts, 
				  3 is shift-invert mode, 
				  4 is buckling mode, 
				  5 is Cayley mode. */

	int ipntr[11]; /* Indicates the locations in the work array workd
				   where the input and output vectors in the
				   callback routine are located. */

	double *workd = new double[3*n];
	double *workl = new double[ncv*(ncv+8)];
	int lworkl = ncv*(ncv+8);
	int info = 0;
	int rvec = 1; // Changed from above
	int *select = new int[ncv];
	double *d = new double[2*ncv];
	double sigma;
	int ierr;
	double *Z = new double[A->rows()];
	do {
		Geex::dsaupd_(&ido, bmat, &n, arpack_type[eigentype], &nev, &tol, resid, 
			&ncv, v, &ldv, iparam, ipntr, workd, workl, 
			&lworkl, &info);

		if ((ido==1)||(ido==-1)) 
			transpose_self_multiply(A, workd+ipntr[0]-1, workd+ipntr[1]-1, Z);
	} while ((ido==1)||(ido==-1));

	delete[] Z;
	if (info<0) 
	{
		std::cout << "Error with dsaupd, info = " << info << "\n";
		std::cout << "Check documentation in dsaupd\n\n";
		assert( false );
	}
	else 
	{
		Geex::dseupd_(&rvec, "All", select, d, v, &ldv, &sigma, bmat, 
			&n, arpack_type[eigentype], &nev, &tol, resid, &ncv, v, &ldv, 
			iparam, ipntr, workd, workl, &lworkl, &ierr);

		if (ierr!=0) 
		{
			std::cout << "Error with dseupd, info = " << ierr << "\n";
			std::cout << "Check the documentation of dseupd.\n\n";
			assert( false );
		} 
		else if (info==1) 
		{
			std::cout << "Maximum number of iterations reached.\n\n";
			assert( false );
		}
		else if (info==3) 
		{
			std::cout << "No shifts could be applied during implicit\n";
			std::cout << "Arnoldi update, try increasing NCV.\n\n";
			assert( false );
		}

		memcpy(eigenvalues, d, sizeof(double)*nev);
		int k = 0;
		for (int i=0; i<nev; i++) 
		{
			eigenvalues[i] = abs(d[i]);//sqrt(d[i]);
			int in = i*n;
			for (int j=0; j<n; j++) 
			{
				eigenvectors[k] = v[in+j];
				k++;
			}
		}

		delete[] resid;
		delete[] workd;
		delete[] workl;
		delete[] select;
		delete[] v;
		delete[] d;
	}

	return true;
}

bool solve_sym_eigensystem_by_ARPACK(Sparse_Matrix *A, int num_of_eigenvalue, int eigentype, 
									 double *eigenvalues, double *eigenvectors)
{
	int n = A->cols();
	int nev = num_of_eigenvalue;
	int ido = 0;
	char bmat[2] = "I";
	double tol = 0.0;
	double *resid = new double[n];
	int ncv = 4*nev;
	if (ncv>n) ncv = n;
	int ldv = n;
	double *v = new double[ldv*ncv];
	int iparam[11]; /* An array used to pass information to the routines
					about their functional modes. */
	iparam[0] = 1; // Specifies the shift strategy (1->exact)

	iparam[2] = 3*n; // Maximum number of iterations

	iparam[6] = 1;/* Sets the mode of dsaupd.
				  1 is exact shifting, 
				  2 is user-supplied shifts, 
				  3 is shift-invert mode, 
				  4 is buckling mode, 
				  5 is Cayley mode. */

	int ipntr[11]; /* Indicates the locations in the work array workd
				   where the input and output vectors in the
				   callback routine are located. */

	double *workd = new double[3*n];
	int lworkl = ncv*(ncv+8);
	double *workl = new double[lworkl];
	int info = 0;
	int rvec = 1; // Changed from above
	int *select = new int[ncv];
	double *d = new double[nev];
	double sigma;
	int ierr;

	do 
	{
		dsaupd_(&ido, bmat, &n, arpack_type[eigentype], &nev, &tol, resid, 
			&ncv, v, &ldv, iparam, ipntr, workd, workl, 
			&lworkl, &info);

		if ((ido==1)||(ido==-1)) 
			multiply(A, workd+ipntr[0]-1, workd+ipntr[1]-1);
	} while ((ido==1)||(ido==-1));

	if (info<0) 
	{
		std::cout << "Error with dsaupd, info = " << info << "\n";
		std::cout << "Check documentation in dsaupd\n\n";
		assert( false );
	}
	else 
	{
		dseupd_(&rvec, "All", select, d, v, &ldv, &sigma, bmat, 
			&n, arpack_type[eigentype], &nev, &tol, resid, &ncv, v, &ldv, 
			iparam, ipntr, workd, workl, &lworkl, &ierr);

		if (ierr!=0) 
		{
			std::cout << "Error with dseupd, info = " << ierr << "\n";
			std::cout << "Check the documentation of dseupd or dneupd.\n\n";
			assert( false );
		} 
		else if (info==1) 
		{
			std::cout << "Maximum number of iterations reached.\n\n";
			assert( false );
		}
		else if (info==3) 
		{
			std::cout << "No shifts could be applied during implicit\n";
			std::cout << "Arnoldi update, try increasing NCV.\n\n";
			assert( false );
		}

		memcpy(eigenvalues, d, sizeof(double)*nev);
		int k = 0;
		for (int i=0; i<nev; i++) 
		{
			int in = i*n;
			for (int j=0; j<n; j++) 
			{
				eigenvectors[k] = v[in+j];
				k++;
			}
		}
	}
	delete[] resid;
	delete[] workd;
	delete[] workl;
	delete[] select;
	delete[] v;
	delete[] d;
	return true;
}

} // namespace Geex

#endif
